import {reactive, toRefs} from "vue";

const length = 9;
const nbEmptyCell = 2;
const nbCellGrid = Math.pow(length, 2);
const nbCellLineSquare = Math.sqrt(length);
const numbers = Array.from({length: length}, (_, k) => k + 1);

function shuffle(array){
    return array
        .map((a) => ({sort: Math.random(), value: a}))
        .sort((a, b) => a.sort - b.sort)
        .map((a) => a.value);
}

const state = reactive({
    grid: null,
    mask: null,
    maskedGrid: null
});

function generateGrid() {
    let grid, lines, columns, squares;

    let grid_completed = false;
    outerwhile:
        while (!grid_completed) {
            // Array creation for the grid
            grid = Array(length)
                .fill()
                .map(
                    () => Array(length)
                        .fill(0)
                );
            // Array creation for the lines
            lines = Array(length)
                .fill()
                .map(
                    () => Array
                        .from(numbers)
                );
            // Array creation for the columns
            columns = Array(length)
                .fill()
                .map(
                    () => Array
                        .from(numbers)
                );
            // Array creation for the squares
            squares = [];
            for (let i = 0; i < nbCellLineSquare; i++) {
                let row = Array(nbCellLineSquare);
                for (let j = 0; j < nbCellLineSquare; j++) {
                    row[j] = Array.from(numbers);
                }
                squares.push(row);
            }

            //populate grille
            for (let y = 0; y < length; y++) {
                for (let x = 0; x < length; x++) {
                    let possibilites = [];
                    numbers.forEach(k => {
                        const line = lines[y];
                        const column = columns[x];
                        const square = squares[Math.floor(y / nbCellLineSquare)][Math.floor(x / nbCellLineSquare)];
                        if (line.includes(k) && column.includes(k) && square.includes(k)) possibilites.push(k);
                    });
                    if (possibilites.length > 0) {
                        let nb = shuffle(possibilites)[0];
                        grid[y][x] = nb;
                        lines[y][nb - 1] = undefined;
                        columns[x][nb - 1] = undefined;
                        squares[Math.floor(y / nbCellLineSquare)][Math.floor(x / nbCellLineSquare)][nb - 1] = undefined;
                    } else {
                        continue outerwhile;
                    }
                }
            }
            grid_completed = true;
        }
    return grid;
}
function generateMask() {
    let res, mask;

    let empty_cell = Array(nbEmptyCell)
        .fill(true)
        .concat(
            Array(nbCellGrid - nbEmptyCell)
                .fill(false)
        );
    let notFair = true;
    while (notFair) {
        mask = shuffle(empty_cell);
        for (let i = 0; i < (mask.length - length); i += length) {
            const line = mask.slice(i, i + length);
            notFair = line.filter(elem => !elem).length <= 2;
            if (notFair) break;
        }
    }
    res = numbers.map(num =>{
        return mask.slice((num-1)*9, num*9);
    });
    return res;
}
function generateMaskedGrid() {
    let res = [];
    if(state.mask){
        state.grid.forEach((row, ir) => {
            res.push([]);
            row.forEach((cell,ic) => {
                if(state.mask[ir][ic]){
                    res[ir][ic] = 0
                }else{
                    res[ir][ic] = cell;
                }
            })});
    }

    return res;
}

export default function useSudoku(){
    const getNewGrid = () => state.grid = generateGrid();
    const getNewMask = () => state.mask = generateMask();
    const getMaskedGrid = () => state.maskedGrid = generateMaskedGrid();
    const isValueCorrect = (value, row, col) => {
        return value === state.grid[row][col];
    };
    const updateCellValue = (value, row, col) => {
      state.maskedGrid[row][col] = value;
    };
    const isGridCompleted = () =>{
        const emptyCells = state.maskedGrid.flat().filter(c => c === 0);
        return emptyCells.length === 0;
    };
    return {...toRefs(state),numbers , getNewGrid, getNewMask, getMaskedGrid, isValueCorrect, updateCellValue, isGridCompleted};
}