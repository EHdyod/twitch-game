import {reactive, toRefs} from "vue";

const score = reactive({
    finish: 0,
    move: 0
});

export default function useState(){
    const incrementPoint = () =>{ score.finish++};
    const incrementMove = () =>{score.move++};
    const resetMove = () =>{score.move = 0};
    return {...toRefs(score) ,incrementMove ,incrementPoint, resetMove}
}